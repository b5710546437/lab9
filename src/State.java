/**
 * State enum use for keep every state of WordCounter.
 * @author Arut Thanomwatana
 *
 */
public enum State {
	START,
	CONSONANT,
	E_FIRST,
	VOWEL,
	NO_WORD,
	DASH
}
