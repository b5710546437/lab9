import java.io.IOException;

/**
 * Main use for test the WordCounter.
 * @author Arut Thanomwatana
 *
 */
public class Main 
{
	/**
	 * Main is use for test word counter and time it.
	 * @param args not used 
	 * @throws IOException if URL is not valid
	 */
	public static void main(String [] args) throws IOException{
		WordCounter wordCount = new WordCounter();
		StopWatch watch = new StopWatch();
		System.out.println("Reading words from http://se.cpe.ku.ac.th/dictionary");
		watch.start();
		System.out.printf("Counted %d syllables in %d words\n",wordCount.getSyllableCount(),wordCount.countWords());
		watch.stop();
		System.out.printf("Elapsed time: %.3f sec",watch.getElapsed());
	}

}
