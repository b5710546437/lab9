import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * A WordCounter that can count syllables and word.
 * @author Arut Thanomwatana
 *
 */
public class WordCounter 
{
	State state = State.START; 

	/**
	 * countSyllables use for count the syllables of word.
	 * @param word is the word that will be count syllables
	 * @return number of syllables in word 
	 */
	public int countSyllables(String word){
		state = State.START;
		String cutWord = word.replaceAll("'","");
		int syllables = 0;
		char [] wordCount = cutWord.toCharArray();
		for(char c : wordCount){
			switch(this.state){
			case START :
				
				if(c=='e'||c=='E') state = State.E_FIRST;
				else if(isVowel(c)) state = State.VOWEL;
				else if(Character.isLetter(c)) state = State.CONSONANT;
				else state = State.NO_WORD;
				break;
			case CONSONANT :
				
				if(c=='e'||c=='E') state = State.E_FIRST;
				else if(isVowel(c)||c=='y'||c=='Y') state = State.VOWEL;
				else if(Character.isLetter(c)) state = State.CONSONANT;
				else if(c=='-') state = State.DASH;
				else state = State.NO_WORD;
				break;
			case VOWEL :
				
				if(isVowel(c)) state = State.VOWEL;
				else if(Character.isLetter(c)){
					syllables++;
					state = State.CONSONANT;
				}
				else if(c=='-'){
					syllables++;
					state = State.DASH;
				}
				else state = State.NO_WORD;
				break;
			case E_FIRST :
				if(isVowel(c)) state = State.VOWEL;
				else if(Character.isLetter(c)) {
					syllables++;
					state = State.CONSONANT;
				}
				else if(c=='-'){
					state = State.DASH;
				}
				else state = State.NO_WORD;
				break;
			case DASH :
				if(c=='e'||c=='E') state = State.E_FIRST;
				else if(isVowel(c)) state = State.VOWEL;
				else if(Character.isLetter(c)) state = State.CONSONANT;
				else state = State.NO_WORD;
				break;
			case NO_WORD :
				break;
			}	
		}
		if(state == State.E_FIRST&&syllables==0)
			syllables++;
		if(state == State.VOWEL)
			syllables++;
		if(state == State.DASH)
			return 0;
		if(state == State.NO_WORD)
			return 0;
		return syllables;
	}
	/**
	 * countWords use for count number of word in the URL.
	 * @return number of word that was counted from URL
	 * @throws IOException if URL is not valid
	 */
	public int countWords() throws IOException{
		int count = 0;
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL(DICT_URL);
		InputStream in = url.openStream();
		Scanner scan = new Scanner(in);
		while(scan.hasNext()){
			count++;
			scan.nextLine();
		}
		return count;

	}
	/**
	 * getSyllableCount use for count every syllable in word in URL.
	 * @return number of syllables in every word that was counted from URL
	 * @throws IOException if URL is not valid
	 */
	public int getSyllableCount() throws IOException{
		int count = 0;
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL(DICT_URL);
		InputStream in = url.openStream();
		Scanner scan = new Scanner(in);
		while(scan.hasNext()){
			count+= this.countSyllables(scan.next());
			scan.nextLine();
		}
		return count;
	}
	/**
	 * isVowel use for check wheter char is vowel or not.
	 * @param c is char that will be check wheter it is a vowel or not
	 * @return true if c is a vowel.
	 */
	public boolean isVowel(char c){
		if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='O'||c=='U'||c=='I'){
			return true;
		}
		return false;
	}


}
